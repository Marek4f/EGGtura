package interfejs;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.text.DefaultEditorKit;

import eggtura.EGGtura;
import program.PobierzKontrahent;


public class okno5 extends JFrame{
	
	public okno5() {

	 final JTextField imieinazwisko = new JTextField();
	 final JTextField adres = new JTextField();
	 final JTextField nip = new JTextField();

	 JButton guzik = new JButton("Zapisz");
	    JPanel panel = new JPanel();
	    JPanel panel2 = new JPanel();

	    panel.setLayout(new GridLayout(20,1));
	    panel2.setLayout(new FlowLayout());

	    int index = okno4.list.getSelectedIndex();
	    panel.add(new JLabel("Imi� i Nazwisko / Nazwa firmy",SwingConstants.CENTER));
	    panel.add(imieinazwisko);
	    panel.add(new JLabel("Adres",SwingConstants.CENTER));
	    panel.add(adres);
	    panel.add(new JLabel("NIP",SwingConstants.CENTER));
	    panel.add(nip);

	    panel2.add(guzik);
	    panel.add(panel2);
	    add(panel, BorderLayout.NORTH);
	   
	    
	    imieinazwisko.setText(PobierzKontrahent.tytul.get(index));
	    adres.setText(PobierzKontrahent.adres.get(okno4.list.getSelectedIndex()));
	    nip.setText(PobierzKontrahent.nip.get(okno4.list.getSelectedIndex()));


	    guzik.addActionListener(new ActionListener()
	    {
	    @SuppressWarnings("unchecked")
		public void actionPerformed(ActionEvent event)
	    {
			PreparedStatement prepStmt = null;
			
			try {
				prepStmt = EGGtura.conn.prepareStatement("UPDATE kontrahent SET tytul=(?),adres=(?),nip=(?) WHERE tytul=(?);");
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				prepStmt.setString(1, imieinazwisko.getText());
				prepStmt.setString(2, adres.getText());
				prepStmt.setString(3, nip.getText());
				prepStmt.setString(4, PobierzKontrahent.tytul.get(index));
				okno4.model.set(index, imieinazwisko.getText()+"     "+adres.getText()+"     "+nip.getText()+" ");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				prepStmt.execute();
				try {
					new PobierzKontrahent();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			dispose();
			}
	    });
	    JPopupMenu popup = new JPopupMenu();
	    JMenuItem item = new JMenuItem(new DefaultEditorKit.CutAction());
	    item.setText("Wytnij");
	    popup.add(item);
	    item = new JMenuItem(new DefaultEditorKit.CopyAction());
	    item.setText("Kopiuj");
	    popup.add(item);
	    item = new JMenuItem(new DefaultEditorKit.PasteAction());
	    item.setText("Wklej");
	    popup.add(item);
	    imieinazwisko.setComponentPopupMenu(popup);
	    adres.setComponentPopupMenu(popup);
	    nip.setComponentPopupMenu(popup);

	    ImageIcon img = new ImageIcon("jajo.jpg");
	    setIconImage(img.getImage());
 		setSize(400, 350);
 		setLocation(300,300);
	    setTitle("Edycja");

	    setLocationByPlatform(true);

 		setVisible(true);  

	}
}
