package interfejs;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Desktop.Action;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Vector;

import interfejs.okno2;
import interfejs.wczytajKontrahenta;

import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.TitledBorder;
import javax.swing.text.DefaultEditorKit;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import com.itextpdf.text.DocumentException;
import com.toedter.calendar.JDateChooser;

import eggtura.EGGtura;
import program.Odswiez;
import program.Oswiez2;
import program.PlikOpcje;
import program.PobierzFirmy;
import program.ProduktUsuwanie;
import program.Faktura;
import interfejs.okno4;
@SuppressWarnings("serial")
public class Interfejs extends JFrame {
	AbstractAction mojedane = new AbstractAction("Dane Firmy") {
        public void actionPerformed(ActionEvent e) {
          Dane();
        }
      };
      private void Dane() {

    	  
				new okno2();
    	  
      }
      AbstractAction klienci = new AbstractAction("Kontrahenci") {
          public void actionPerformed(ActionEvent e) {
        	  Klienci();
          }
        };
        private void Klienci() {

      	  
      	  if (c==0)
  				new okno4();
      	
      	  else {
      		  okno4.tablka.setVisible(true);
      		  new Oswiez2();
      	  }
      	    c=1;
        }
        
      AbstractAction opcje = new AbstractAction("Opcje") {
          public void actionPerformed(ActionEvent e) {
            Opcje();
          }
        };
        private void Opcje() {

      	  
  				new okno3();
      	  
        }
        
	    static int rokint = Calendar.getInstance().get(Calendar.YEAR);
	   static int rokint2=rokint+1;
	   static int rokint3=rokint-1;


    	public static String[] w = {"PLN", "EUR"};
    	
	static String[] platnosc = {"Przelew", "Got�wka"};
	public static	JSpinner nrfaktury = new JSpinner();
		 public static JComboBox waluta;
			public static DefaultListModel model = new DefaultListModel();
			public static JList<String> lista = new JList<String>(model);
			public static JTextField imie = new JTextField();
			public static JTextField adres = new JTextField();
			public static JTextField nip = new JTextField();

	    public static JComboBox formapl;
	    public static JComboBox firma;
	    public static PopupMenu popup;
	    public static JDateChooser dateChooser1;
	    public static JDateChooser dateChooser2;
	    public static JDateChooser dateChooser3;


	    
	    public static	JSpinner zali = new JSpinner();
	   
	    public int  c= 0;
	    public int  cc= 0;



public Interfejs(){
	try {
		UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
	} catch (ClassNotFoundException | InstantiationException
			| IllegalAccessException | UnsupportedLookAndFeelException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	nrfaktury.setModel(new javax.swing.SpinnerNumberModel(1,1, 10000000, 1));

    zali.setModel(new javax.swing.SpinnerNumberModel(0,0, 10000000, 0.01));

    lista.setFixedCellHeight(12);
    JScrollPane scrollPane1 = new JScrollPane(lista);


    formapl = new JComboBox(platnosc);
    firma = new JComboBox(PobierzFirmy.firmy);
    waluta = new JComboBox(w);
    JPopupMenu popup = new JPopupMenu();
    JMenuItem item = new JMenuItem(new DefaultEditorKit.CutAction());
    item.setText("Wytnij");
    popup.add(item);
    item = new JMenuItem(new DefaultEditorKit.CopyAction());
    item.setText("Kopiuj");
    popup.add(item);
    item = new JMenuItem(new DefaultEditorKit.PasteAction());
    item.setText("Wklej");
    popup.add(item);
    imie.setComponentPopupMenu(popup);
    nip.setComponentPopupMenu(popup);
    adres.setComponentPopupMenu(popup);

    
	    JPanel panelgora = new JPanel();
	    panelgora.setLayout(new GridLayout(14,1));
	    
	    JPanel panel1 = new JPanel();
	    panel1.setLayout(new GridLayout(1,2));
	    
	    JPanel panel2 = new JPanel();
	    panel2.setLayout(new GridLayout(1,3));
	    
	    JPanel panel3 = new JPanel();
	    panel3.setLayout(new GridLayout(1,3));
	    
	    JPanel panel4 = new JPanel();
	    panel4.setLayout(new GridLayout(1,3));

	    JPanel panel5 = new JPanel();
	    panel5.setLayout(new GridLayout(1,2));

	    JPanel panel6 = new JPanel();
	    panel6.setLayout(new FlowLayout(1,50,1));

	    JPanel panel7 = new JPanel();
	    panel7.setLayout(new GridLayout(1,3));
	    
	    JPanel panel8 = new JPanel();
	    panel8.setLayout(new GridLayout(1,3));
	    
	    JPanel panel9 = new JPanel();
	    panel9.setLayout(new GridLayout(1,1));
	    
	    JPanel panel10 = new JPanel();
	    panel10.setLayout(new GridLayout(1,1));
	    
	    JPanel panel11 = new JPanel();
	    panel11.setLayout(new GridLayout(1,1));
	    
	    JPanel panelsrodek = new JPanel();
	    panelsrodek.setLayout(new GridLayout(3,1));
	    
	    JPanel paneldol = new JPanel();
	    paneldol.setLayout(new FlowLayout(1,150,1));


	    JPanel pusty = new JPanel();
	    pusty.setLayout(new GridLayout(1,1));
	    
	    JPanel pusty2 = new JPanel();
	    pusty2.setLayout(new GridLayout(1,1));
	    
	    JPanel pusty3 = new JPanel();
	    pusty3.setLayout(new GridLayout(1,1));
	    
	    JPanel pusty4 = new JPanel();
	    pusty4.setLayout(new GridLayout(1,1));
	    
	    JPanel pusty5 = new JPanel();
	    pusty5.setLayout(new GridLayout(1,1));
	    
	    JPanel pusty6 = new JPanel();
	    pusty6.setLayout(new GridLayout(1,2));
	    
	    JPanel pusty7 = new JPanel();
	    pusty7.setLayout(new GridLayout(1,1));
	    
	    JPanel pusty8 = new JPanel();
	    pusty8.setLayout(new FlowLayout(1,150,1));
	    
	    JPanel pusty9 = new JPanel();
	    pusty9.setLayout(new GridLayout(1,1));
	    
	    JPanel pusty10 = new JPanel();
	    pusty10.setLayout(new GridLayout(1,1));
	    
	    JPanel pusty11 = new JPanel();
	    pusty11.setLayout(new GridLayout(1,1));
	    
	    JPanel napisy = new JPanel();
	    napisy.setLayout(new GridLayout(1,2));
	    	    
	    napisy.add(new JLabel(" Nr faktury",SwingConstants.LEFT));
	    napisy.add(new JLabel("Data wystawienia ",SwingConstants.RIGHT));
	    
	    JPanel napisy2 = new JPanel();
	    napisy2.setLayout(new GridLayout(1,2));

	    napisy2.add(new JLabel(" Data sprzeda�y",SwingConstants.LEFT));
	    napisy2.add(new JLabel("Termin p�atno�ci ",SwingConstants.RIGHT));
	    
	    JPanel napisy3 = new JPanel();
	    napisy3.setLayout(new GridLayout(1,2));

	    napisy3.add(new JLabel(" Imi� i nazwisko / nazwa firmy",SwingConstants.LEFT));
	    napisy3.add(new JLabel("Adres ",SwingConstants.RIGHT));
	    
	    JPanel napisy4 = new JPanel();
	    napisy4.setLayout(new GridLayout(1,2));

	    napisy4.add(new JLabel(" NIP",SwingConstants.LEFT));
	    napisy4.add(new JLabel("Forma p�atno�ci ",SwingConstants.RIGHT));
	    JPanel napisy5 = new JPanel();
	    napisy5.setLayout(new GridLayout(1,2));
	    napisy5.add(new JLabel(" Waluta",SwingConstants.LEFT));
	    napisy5.add(new JLabel("Kwota op�acona ",SwingConstants.RIGHT));
	    
	    JPanel napisy6 = new JPanel();
	    napisy6.setLayout(new GridLayout(1,1));
	    napisy6.add(new JLabel("Wybierz firm� wystawiaj�c� faktur�",SwingConstants.CENTER));
	    
	    JPanel napisy7 = new JPanel();
	    napisy7.setLayout(new GridLayout(1,1));
	    
	    JMenu menu = new JMenu("Program");
	    JMenuBar belka = new JMenuBar();
	    
		 JButton dodaj = new JButton("Dodaj produkt");
		 JButton usun = new JButton("Usu� produkt");
		 JButton usunWszystko = new JButton("Usu� wszystko");

		 JButton zapiszfakture = new JButton("Zapisz Faktur�");
		 JButton wczytaj = new JButton("Wczytaj kontrahenta");
		 dateChooser1 = new JDateChooser();
		 dateChooser2 = new JDateChooser();
		 dateChooser3 = new JDateChooser();
		 
	    belka.add(menu);
	    setJMenuBar(belka);
	    menu.add(mojedane);
	    menu.add(klienci);
	    menu.add(opcje);


		panel10.add(napisy6);
		panel11.add(firma);

		panelgora.add(panel10);
		panelgora.add(panel11);

	    panel1.add(nrfaktury);
	    panel2.add(dateChooser1);

	    
	    panel3.add(dateChooser2);


	   panel3.add(pusty2);
	   panel3.add(dateChooser3);

	   panelgora.add(napisy);
	   panel1.add(pusty);
	   panel1.add(panel2);

	    panelgora.add(panel1);
	   panelgora.add(napisy2);

	   panelgora.add(panel3);
	   panelgora.add(napisy3);
	   
	   panel4.add(imie);
	    panel4.add(pusty3);
	    panel4.add(adres);
		panelgora.add(panel4);
		   
		panelgora.add(napisy4);
		
		panel5.add(nip);
	    panel5.add(pusty4);
	    panel5.add(formapl);
	    
		panelgora.add(panel5);
		panelgora.add(napisy5);
		panel7.add(waluta);
		panel7.add(pusty6);
		panel7.add(zali);
		panelgora.add(panel7);
		panel8.add(pusty11);
		panel9.add(pusty9);
		panel9.add(wczytaj);
		panel9.add(pusty10);
		
		
		panelgora.add(panel8);
		panelgora.add(panel9);

		panelsrodek.add(scrollPane1);


	    panel6.add(dodaj);
	    panel6.add(usun);
	    panel6.add(usunWszystko);

	    pusty8.add(zapiszfakture);
		panelsrodek.add(panel6);
		panelsrodek.add(pusty8);


		TitledBorder ramka = new TitledBorder("Dane");
		panelgora.setBorder(ramka);

	    add(panelgora, BorderLayout.NORTH);
	    add(panelsrodek, BorderLayout.CENTER);
	    //add(paneldol, BorderLayout.SOUTH);
	    
	    dodaj.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent event)
	    {
			
			new dodawanie();

	        
	    }
	    });
	    usun.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent event)
	    {
			
			new ProduktUsuwanie();

	        
	    }
	    });
	    usunWszystko.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent event)
	    {
			model.removeAllElements();	  
			EGGtura.listaproduktow.removeAll(EGGtura.listaproduktow);
			EGGtura.listacen.removeAll(EGGtura.listacen);
			EGGtura.listasztuk.removeAll(EGGtura.listasztuk);
			EGGtura.listavat.removeAll(EGGtura.listavat);
			EGGtura.listajm.removeAll(EGGtura.listajm);
			EGGtura.numer=0;
	    }
	    });
	    wczytaj.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent event)
	    {
			if (cc==0)
			new wczytajKontrahenta();
			else {
				wczytajKontrahenta.tablka2.setVisible(true);
				new Odswiez();
			}
			cc=1;
	        
	    }
	    });
	    zapiszfakture.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent event)
	    {
			
				try {
					new Faktura();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

	    }
	    });
	    ImageIcon img = new ImageIcon("jajo.jpg");
	    setIconImage(img.getImage());

	    Toolkit ekran = Toolkit.getDefaultToolkit();
	    Dimension rozmiar = ekran.getScreenSize();
	    int wysokosc = rozmiar.height ;
	    int szerokosc = rozmiar.width;
	    setSize(szerokosc / 3, wysokosc - 70);
	    setLocationByPlatform(true);
	    setLocationRelativeTo(null);
	    setTitle("EGGtura");
	    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);

}

}

