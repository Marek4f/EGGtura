package interfejs;


import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import eggtura.EGGtura;
import program.PobierzKontrahent;
import program.ProduktUsuwanie;

@SuppressWarnings("serial")
public class wczytajKontrahenta extends JPanel {

  public static JList<Object> list2;

  public static DefaultListModel<Object> model2;

	public static JFrame tablka2 = new JFrame("Kontrahenci");

  public wczytajKontrahenta() {
	  try {
			new PobierzKontrahent();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    setLayout(new BorderLayout());
    model2 = new DefaultListModel<Object>();
    list2 = new JList<Object>(model2);
    JScrollPane pane = new JScrollPane(list2);
    JButton editButton = new JButton("Wczytaj kontrahenta");
    tablka2.add(pane, BorderLayout.NORTH);
    tablka2.add(editButton, BorderLayout.CENTER);
int e=0;
    for (int gg = 0; gg < PobierzKontrahent.tytul.size(); gg++)
      model2.addElement(PobierzKontrahent.tytul.get(gg)+"     "+PobierzKontrahent.adres.get(gg)+"     "+PobierzKontrahent.nip.get(gg)+" ");

    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
    	  if (list2.getSelectedIndex()!=-1){
    		  Interfejs.imie.setText(PobierzKontrahent.tytul.get(list2.getSelectedIndex()));
    		  Interfejs.adres.setText(PobierzKontrahent.adres.get(list2.getSelectedIndex()));
    		  Interfejs.nip.setText(PobierzKontrahent.nip.get(list2.getSelectedIndex()));
    		  tablka2.setVisible(false);

    	  }
        
      }
    });
   

    ImageIcon img = new ImageIcon("jajo.jpg");
    tablka2.setIconImage(img.getImage());
    tablka2.setSize(600, 250);
    tablka2.setLocationRelativeTo(null);

    tablka2.setVisible(true);
  }


}
