package interfejs;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.text.DefaultEditorKit;

import eggtura.EGGtura;
import program.PlikOpcje;
import program.PobierzFirmy;
import program.PobierzKontrahent;
import program.ProduktUsuwanie;

public class okno2 extends JFrame{
	
	public okno2() {
		try {
			new PlikOpcje();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    JComboBox fir = new JComboBox(PobierzFirmy.firmy);

	 final JTextField imieinazwisko = new JTextField();
	 final JTextField adres = new JTextField();
	 final JTextField nip = new JTextField();
	 final JTextField telefon = new JTextField();
	 final JTextField mail = new JTextField();
	 final JTextField banknazwa = new JTextField();
	 final JTextField banknr = new JTextField();
	 JButton guzik = new JButton("Zapisz zmiany");
	 JButton guzik2 = new JButton("Dodaj now� firm�");
	 JButton guzik3 = new JButton("Usu� wybran� firm�");

	    JPanel panel = new JPanel();
	    JPanel panel2 = new JPanel();

	    panel.setLayout(new GridLayout(20,1));
	    panel2.setLayout(new FlowLayout());

	    panel.add(new JLabel("Wybierz firm�",SwingConstants.CENTER));
	    panel.add(fir);
	    panel.add(new JLabel("Imi� i Nazwisko / Nazwa firmy",SwingConstants.CENTER));
	    panel.add(imieinazwisko);
	    panel.add(new JLabel("Adres",SwingConstants.CENTER));
	    panel.add(adres);
	    panel.add(new JLabel("NIP",SwingConstants.CENTER));
	    panel.add(nip);
	    panel.add(new JLabel("Telefon",SwingConstants.CENTER));
	    panel.add(telefon);
	    panel.add(new JLabel("E-mail",SwingConstants.CENTER));
	    panel.add(mail);
	    panel.add(new JLabel("Nazwa Banku",SwingConstants.CENTER));
	    panel.add(banknazwa);
	    panel.add(new JLabel("Nr konta",SwingConstants.CENTER));
	    panel.add(banknr);
	    panel2.add(guzik);
	    panel2.add(guzik2);
	    panel2.add(guzik3);

	    panel.add(panel2);
	    add(panel, BorderLayout.NORTH);
	   
	    

	    guzik.addActionListener(new ActionListener()
	    {
	    public void actionPerformed(ActionEvent event)
	    {
PreparedStatement prepStmt = null;
			
			try {
				prepStmt = EGGtura.conn.prepareStatement("UPDATE firma SET nazwa=(?), adres=(?), nip=(?), telefon=(?), mail=(?), bank=(?), bank_nr=(?)  WHERE nazwa=(?);");
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				prepStmt.setString(1, imieinazwisko.getText());
				prepStmt.setString(2, adres.getText());
				prepStmt.setString(3, nip.getText());
				prepStmt.setString(4, telefon.getText());
				prepStmt.setString(5, mail.getText());
				prepStmt.setString(6, banknazwa.getText());
				prepStmt.setString(7, banknr.getText());
				prepStmt.setString(8, fir.getSelectedItem().toString());
				fir.removeAllItems();
				Interfejs.firma.removeAllItems();
				} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				prepStmt.execute();

				int j = 0;
				while (PobierzFirmy.nazwa.size()>j){
					fir.addItem(PobierzFirmy.nazwa.get(j));
					Interfejs.firma.addItem(PobierzFirmy.nazwa.get(j));

					j++;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	      }  	 
	    });
	    guzik2.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent event)
	    {
			
PreparedStatement prepStmt = null;
			
			try {
				prepStmt = EGGtura.conn.prepareStatement("insert into firma values (NULL ,?, ?, ?, ?, ?, ?, ?);");
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				prepStmt.setString(1, imieinazwisko.getText());
				prepStmt.setString(2, adres.getText());
				prepStmt.setString(3, nip.getText());
				prepStmt.setString(4, telefon.getText());
				prepStmt.setString(5, mail.getText());
				prepStmt.setString(6, banknazwa.getText());
				prepStmt.setString(7, banknr.getText());
				fir.removeAllItems();
				Interfejs.firma.removeAllItems();
				try {
					new PobierzFirmy();
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				prepStmt.execute();
				int j = 0;
				while (PobierzFirmy.nazwa.size()>j){
					fir.addItem(PobierzFirmy.nazwa.get(j));
					Interfejs.firma.addItem(PobierzFirmy.nazwa.get(j));

					j++;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	    }
	    });
	    guzik3.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent event)
	    {
			
			PreparedStatement prepStmt = null;
			try {
				prepStmt = EGGtura.conn.prepareStatement("DELETE FROM firma where nazwa=?;");
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				prepStmt.setString(1, fir.getSelectedItem().toString());
				fir.removeAllItems();
				Interfejs.firma.removeAllItems();

				try {
					new PobierzFirmy();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				prepStmt.execute();
				int j = 0;
				while (PobierzFirmy.nazwa.size()>j){
					fir.addItem(PobierzFirmy.nazwa.get(j));
					Interfejs.firma.addItem(PobierzFirmy.nazwa.get(j));

					j++;
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	    });
	    fir.addActionListener (new ActionListener () {
	        public void actionPerformed(ActionEvent e) {
	        	try {
					new PobierzFirmy();
					
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	        	if (fir.getItemCount()!=0){
	        		if (fir.getSelectedIndex()!=-1){
	           adres.setText(PobierzFirmy.adres.get(fir.getSelectedIndex()));
	           nip.setText(PobierzFirmy.nip.get(fir.getSelectedIndex()));
	           imieinazwisko.setText(PobierzFirmy.nazwa.get(fir.getSelectedIndex()));
	           telefon.setText(PobierzFirmy.tel.get(fir.getSelectedIndex()));
	           mail.setText(PobierzFirmy.mail.get(fir.getSelectedIndex()));
	           banknazwa.setText(PobierzFirmy.bank.get(fir.getSelectedIndex()));
	           banknr.setText(PobierzFirmy.bankNr.get(fir.getSelectedIndex()));
	        		}
	        	}
	        }
	    });
	    
	    JPopupMenu popup = new JPopupMenu();
	    JMenuItem item = new JMenuItem(new DefaultEditorKit.CutAction());
	    item.setText("Wytnij");
	    popup.add(item);
	    item = new JMenuItem(new DefaultEditorKit.CopyAction());
	    item.setText("Kopiuj");
	    popup.add(item);
	    item = new JMenuItem(new DefaultEditorKit.PasteAction());
	    item.setText("Wklej");
	    popup.add(item);
	    imieinazwisko.setComponentPopupMenu(popup);
	    adres.setComponentPopupMenu(popup);
	    telefon.setComponentPopupMenu(popup);
	    mail.setComponentPopupMenu(popup);
	    banknazwa.setComponentPopupMenu(popup);
	    banknr.setComponentPopupMenu(popup);
	    nip.setComponentPopupMenu(popup);

	    ImageIcon img = new ImageIcon("jajo.jpg");
	    setIconImage(img.getImage());
 		setSize(500, 700);
 		setLocation(300,300);
	    setTitle("Dane");

	    setLocationByPlatform(true);

 		setVisible(true);  
}
}
