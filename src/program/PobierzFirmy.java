package program;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import eggtura.EGGtura;
import program.Kontrahent;
public class PobierzFirmy {
	public static ArrayList<String> nazwa = new ArrayList<String>();
	public static ArrayList<String> adres = new ArrayList<String>();
	public static ArrayList<String> nip = new ArrayList<String>();
	public static ArrayList<String> tel = new ArrayList<String>();
	public static ArrayList<String> mail = new ArrayList<String>();
	public static ArrayList<String> bank = new ArrayList<String>();
	public static ArrayList<String> bankNr = new ArrayList<String>();
	public static String[] firmy = new String[10];

	public static ArrayList<Integer> id = new ArrayList<Integer>();

	public PobierzFirmy() throws IOException{
			ResultSet result = null;
			nazwa.removeAll(nazwa);
	         adres.removeAll(adres);
	         nip.removeAll(nip);
	         tel.removeAll(tel);
	         mail.removeAll(mail);
	         bank.removeAll(bank);
	         bankNr.removeAll(bankNr);

			try {
				result = EGGtura.stat.executeQuery("SELECT * FROM firma");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			int l=0;

	            try {
					while(result.next()) {
					    id.add(result.getInt("id_firma"));
					    nazwa.add(result.getString("nazwa"));
					    adres.add(result.getString("adres"));
					    nip.add(result.getString("nip"));
					    tel.add(result.getString("telefon"));
					    mail.add(result.getString("mail"));
					    bank.add(result.getString("bank"));
					    bankNr.add(result.getString("bank_nr"));
					    firmy[l]=nazwa.get(l);
					    l++;
					}

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            
	}
	
}
