package program;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.JOptionPane;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.VerticalPositionMark;

import eggtura.EGGtura;
import interfejs.Interfejs;
import interfejs.Progres;
import interfejs.dodawanie;
import interfejs.wczytajKontrahenta;
 
public class Faktura {
	BaseFont czcionka;
	BaseFont czcionka2;

public Faktura() throws IOException{
	new Progres();
	Date date = Interfejs.dateChooser1.getDate();
	Date date2 = Interfejs.dateChooser2.getDate();
    Date date3 = Interfejs.dateChooser3.getDate();

	System.out.println(date);
	if (date==null||date2==null||date3==null){
		Progres.frame.setVisible(false);
		JOptionPane.showMessageDialog(null, "Podaj wszystkie daty");
		return;
	}
    int rokwy=date.getYear()-100+2000;
    int miewy=date.getMonth()+1;
    int rokpl=date2.getYear()-100+2000;
    int miepl=date2.getMonth()+1;
    int rokter=date3.getYear()-100+2000;
    int mieter=date3.getMonth()+1;
    int iledni=0;

    	if (date.getMonth()==date3.getMonth()){
    		iledni=date3.getDate()-date.getDate();

    	}
    	else if (date.getMonth()<date3.getMonth()){
    		int roz=date3.getMonth()-date.getMonth();
    		int ll=0;
    		while (roz>0){
    			GregorianCalendar mycal = new GregorianCalendar(rokwy,date.getMonth()+ll, 1);
    		int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);
    		iledni=iledni+daysInMonth-date.getDate()+date3.getDate();
    		roz--;
    		ll++;
    		}
    		
    		
    	}
    	else if (date.getMonth()>date3.getMonth()){
    		int roz=date.getMonth();
    		int ll=0;
    		while (roz<=11){
    			GregorianCalendar mycal = new GregorianCalendar(rokwy,date.getMonth()+ll, 1);
    			int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);
        		iledni=iledni+daysInMonth-date.getDate()+date3.getDate();
        		roz++;
        		ll++;
    		}
    		 roz=date3.getMonth()-date.getMonth();
    		 ll=0;
    		while (roz>0){
    			GregorianCalendar mycal = new GregorianCalendar(rokwy,date.getMonth()+ll, 1);
    		int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);
    		iledni=iledni+daysInMonth-date.getDate()+date3.getDate();
    		roz--;
    		ll++;
    		}
    		System.out.println(iledni);
    	}
    	
    
    Document dokument = new Document();
    try {
        PdfWriter.getInstance(dokument,
            new FileOutputStream(PlikOpcje.sciezka+"/"+Interfejs.imie.getText()+" "+date2.getDate()+"-"+
            		miepl+"-"+rokpl+".pdf"));

        dokument.open();

        for (int pp = 0; pp < Interfejs.imie.getText().length(); pp++) {
        	czcionka2 = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.EMBEDDED);
            if (Interfejs.imie.getText().toString().substring(pp,pp+1).equals("Ă…")==true||Interfejs.imie.getText().toString().substring(pp,pp+1).equals("ĂĄ")==true||Interfejs.imie.getText().toString().substring(pp,pp+1).equals("Ă¸")==true||Interfejs.imie.getText().toString().substring(pp,pp+1).equals("Ă�")==true)
            	 czcionka2 = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.EMBEDDED);
            else if (Interfejs.imie.getText().toString().substring(pp,pp+1).equals("Ĺ˛")==true||Interfejs.imie.getText().toString().substring(pp,pp+1).equals("Ĺł")==true)
            	czcionka2 = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1257, BaseFont.EMBEDDED);
            else
            	czcionka2 = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.EMBEDDED);

        }
        czcionka = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.EMBEDDED);

        Font pogrubione=new Font(czcionka,12, Font.BOLD);
        Font chude=new Font(czcionka,12);
        Font pogrubione2=new Font(czcionka2,14,Font.BOLD);
        Font pogrubione3=new Font(czcionka2,13,Font.BOLD);
        Font ww=new Font(czcionka);

        
                //String nrf= interfejs.Interfejs.nrfaktury.getSelectedItem();

        Phrase  wpisywanie= new Phrase ();
        Chunk test=new Chunk("Ä…Ä™Ĺ›ĹşÄ‡ĹĽĂł",chude);
        

//System.out.println(date.getYear()-100+2000);
//System.out.println(date.getDate());
        
       Chunk glue = new Chunk(new VerticalPositionMark()); 
       Chunk chunk1 = new Chunk("                                               Faktura VAT numer: "+Interfejs.nrfaktury.getValue().toString()+"/"+miepl+"/"+rokpl+"\n\n",pogrubione2);
        Paragraph dopis1=new Paragraph();


  dopis1.add(chunk1);
  dopis1.add(glue);
        Paragraph dopis3=new Paragraph("Data wystawienia: "+date.getDate()+"-"+
        		miewy+"-"+rokwy+"\n",pogrubione3);
        
        Paragraph glue2 = new Paragraph("Data sprzeda�y: "+date2.getDate()+"-"+
        		miepl+"-"+rokpl+"\n",pogrubione3);
        dopis3.setAlignment(Paragraph.ALIGN_RIGHT);
        Paragraph dopis5=new Paragraph();
        dopis5.setAlignment(Element.ALIGN_RIGHT);
        dopis5.add(glue);
        dopis5.add(glue2);
        Paragraph dopis7=new Paragraph("\n\n\n\nTermin p�atno�ci: ",pogrubione);
        Paragraph dopis8=new Paragraph(+date3.getDate()+"-"+
        		mieter+"-"+rokter+"\n",chude);
        Paragraph dopis9=new Paragraph("P�atno��: ",pogrubione);
        Paragraph dopis99=new Paragraph("\n\nKwota op�acona: ",pogrubione);
        //Interfejs.zali.getValue()
        Paragraph dopis992=new Paragraph(String.format( "%.2f", Interfejs.zali.getValue() ) + " "+ Interfejs.waluta.getSelectedItem(),chude);
        Paragraph dopis10=new Paragraph(Interfejs.formapl.getSelectedItem().toString(),chude);
                Paragraph dopis11 = new Paragraph("Sprzedawca\n", pogrubione2);

        Paragraph dopis111=new Paragraph(PobierzFirmy.nazwa.get(Interfejs.firma.getSelectedIndex())+"\n"+PobierzFirmy.adres.get(Interfejs.firma.getSelectedIndex())+"\n"+PobierzFirmy.nip.get(Interfejs.firma.getSelectedIndex())
		+"\n"+PobierzFirmy.tel.get(Interfejs.firma.getSelectedIndex())+"\n"+PobierzFirmy.bank.get(Interfejs.firma.getSelectedIndex())+"\n"+PobierzFirmy.bankNr.get(Interfejs.firma.getSelectedIndex()), chude);
        

        Paragraph dopis12=new Paragraph("Nabywca\n",pogrubione2);
        Paragraph nowy=new Paragraph("Ilo�� dni do zap�aty: ",pogrubione);
        Paragraph nowy2=new Paragraph(""+iledni,chude);
        Paragraph dopis122=new Paragraph((String) Interfejs.imie.getText()+"\n"+Interfejs.adres.getText()+"\n"+Interfejs.nip.getText(),chude);

        
        Paragraph dopis13=new Paragraph("LP",chude);
        Paragraph dopis14=new Paragraph("Nazwa produktu/us�ugi",chude);
        Paragraph dopis15=new Paragraph("j.m.",chude);
        Paragraph dopis16=new Paragraph("Ilo��",chude);
        Paragraph dopis17=new Paragraph("Cena netto",chude);
        Paragraph dopis18=new Paragraph("Warto�� nettto",chude);
        Paragraph dopis19=new Paragraph("VAT %",chude);


        Chunk dopis20=new Chunk("Warto�� VAT:",chude);
        Chunk dopis21=new Chunk("Warto�� brutto:",chude);
        Chunk dopis22=new Chunk("Warto�� netto:\nVAT\nWarto�� brutto:",chude);
        Chunk dopis24=new Chunk("\nDo zap�aty:",chude);
        Chunk dopis27=new Chunk("    potwierdzenie odbioru lub inne adnotacje\n\n\n\n\n\n",chude);
        Chunk dopis28=new Chunk("               podpis wystawcy faktury",chude);


        PdfPTable tabela1=new PdfPTable(2);
        tabela1.setTotalWidth(500);
        tabela1.setLockedWidth(true);
        PdfPCell tabeladane = new PdfPCell(new Phrase(dopis11));   
        PdfPCell tabeladane3 = new PdfPCell(new Phrase(dopis111));   
        PdfPCell tabeladane33 = new PdfPCell(new Phrase());
        PdfPCell tabeladane333 = new PdfPCell(new Phrase());
        //tabeladane = new PdfPCell();

        PdfPCell tabeladane2 = new PdfPCell(new Phrase(dopis12));  
        tabela1.setWidths(new int[]{280,280});
        PdfPCell tabeladane4 = new PdfPCell(new Phrase(dopis122));   
        tabeladane.setBorder(Rectangle.NO_BORDER);
        tabeladane2.setBorder(Rectangle.NO_BORDER);
        tabeladane3.setBorder(Rectangle.NO_BORDER);
        tabeladane333.setBorder(Rectangle.NO_BORDER);
        tabeladane33.setBorder(Rectangle.NO_BORDER);
        tabeladane4.setBorder(Rectangle.NO_BORDER);

       // tabeladane2 = new PdfPCell();
        
        
        PdfPTable table=new PdfPTable(9);
        table.setTotalWidth(550);
        table.setLockedWidth(true);

        PdfPCell cell1 = new PdfPCell(); 
        cell1=new PdfPCell(new Phrase(dopis13));
        table.addCell(cell1);
        PdfPCell cell2 = new PdfPCell(); 
        cell2=new PdfPCell(new Phrase(dopis14));
        table.addCell(cell2);
        PdfPCell cell3 = new PdfPCell(); 
        cell3=new PdfPCell(new Phrase(dopis15));
        table.addCell(cell3);
        PdfPCell cell4 = new PdfPCell(); 
        cell4=new PdfPCell(new Phrase(dopis16));
        table.addCell(cell4);
        PdfPCell cell5 = new PdfPCell(); 
        cell5=new PdfPCell(new Phrase(dopis17));
        table.addCell(cell5);
        PdfPCell cell6 = new PdfPCell(); 
        cell6=new PdfPCell(new Phrase(dopis18));
        table.addCell(cell6);
        PdfPCell cell7 = new PdfPCell(); 
        cell7=new PdfPCell(new Phrase(dopis19));
        table.addCell(cell7);
        PdfPCell cell8 = new PdfPCell(); 
        cell8=new PdfPCell(new Phrase(dopis20));
        table.addCell(cell8);
        PdfPCell cell9 = new PdfPCell(); 
        cell9=new PdfPCell(new Phrase(dopis21));
        table.addCell(cell9);
        PdfPCell cell19 = new PdfPCell(); 

        ArrayList<Double> wartoscnetto = new ArrayList<Double>();
        ArrayList<Double> wartoscvat = new ArrayList<Double>();
        ArrayList<Double> wartoscbrutto = new ArrayList<Double>();

        for (int i=0;i<EGGtura.numer;i++){
        	PdfPCell cell10 = new PdfPCell(); 
        	int liczba=i+1;
        	Chunk dopistab1=new Chunk(""+liczba+"",chude);
        	cell10=new PdfPCell(new Phrase(dopistab1));
            table.addCell(cell10);
        	PdfPCell cell11 = new PdfPCell(); 
        	Chunk dopistab2=new Chunk(EGGtura.listaproduktow.get(i),chude);
        	cell11=new PdfPCell(new Phrase(dopistab2));
            table.addCell(cell11);
            PdfPCell cell12 = new PdfPCell(); 
        	Chunk dopistab3=new Chunk(EGGtura.listajm.get(i).toString(),chude);
        	cell12=new PdfPCell(new Phrase(dopistab3));
            table.addCell(cell12);       
            PdfPCell cell13 = new PdfPCell(); 
        	Chunk dopistab4=new Chunk(String.format( "%.2f", EGGtura.listasztuk.get(i) ),chude);
        	cell13=new PdfPCell(new Phrase(dopistab4));
            table.addCell(cell13);             
            PdfPCell cell14 = new PdfPCell(); 
            Double a=EGGtura.listacen.get(i);
        	a *= 100; 
            a = (double) Math.round(a);
            a /= 100; 
        	Chunk dopistab5=new Chunk(String.format( "%.2f", a ),chude);
        	cell14=new PdfPCell(new Phrase(dopistab5));
        	table.addCell(cell14);
        	PdfPCell cell15 = new PdfPCell(); 
        	
        	
        	wartoscnetto.add(EGGtura.listacen.get(i)*EGGtura.listasztuk.get(i));
        	Double b=wartoscnetto.get(i);
        	b *= 100; 
            b = (double) Math.round(b);
            b /= 100; 
        	Chunk dopistab6=new Chunk(String.format( "%.2f", b ),chude);
        	cell15=new PdfPCell(new Phrase(dopistab6)); 
        	table.addCell(cell15);
        	PdfPCell cell16 = new PdfPCell(); 
        	Chunk dopistab7=new Chunk(EGGtura.listavat.get(i).toString() +"%",chude);  	
        	cell16=new PdfPCell(new Phrase(dopistab7));
        	table.addCell(cell16);   
        	
        	PdfPCell cell17 = new PdfPCell(); 
        	wartoscvat.add((wartoscnetto.get(i)*EGGtura.listavat.get(i)/100));
        	Double c=wartoscvat.get(i);
        	c *= 100; 
            c = (double) Math.round(c);
            c /= 100; 
            String wartoscvatu=c.toString();
        	Chunk dopistab8=new Chunk(String.format( "%.2f", c ),chude);  	
        	cell17=new PdfPCell(new Phrase(dopistab8));
        	table.addCell(cell17);
        	
        	PdfPCell cell18 = new PdfPCell(); 
        	wartoscbrutto.add(wartoscnetto.get(i)+wartoscvat.get(i));
        	Double d=wartoscbrutto.get(i);
        	d *= 100; 
            d = (double) Math.round(d);
            d /= 100; 
        	Chunk dopistab9=new Chunk(String.format( "%.2f", d ),chude);  	
        	cell18=new PdfPCell(new Phrase(dopistab9));
        	table.addCell(cell18);
        	

        }
        table.setWidths(new int[]{5,20,8,8,10,10,6,10,10});
        PdfPTable tabela2=new PdfPTable(2);
        tabela2.setHorizontalAlignment(Element.ALIGN_RIGHT);


        PdfPCell tabela2dane1 = new PdfPCell();
        PdfPCell tabela2dane2 = new PdfPCell();
        tabela2dane1 = new PdfPCell(new Phrase(dopis22));
        Double e=(double) 0,f=(double) 0,g=(double) 0;
        for(int liczsum=0;liczsum<EGGtura.numer;liczsum++){
        	e+=wartoscnetto.get(liczsum);
        	f+=wartoscvat.get(liczsum);
        	g+=wartoscbrutto.get(liczsum);


        }
        e *= 100; 
        e = (double) Math.round(e);
        e /= 100; 
        f *= 100; 
        f = (double) Math.round(f);
        f /= 100; 
        g *= 100; 
        g = (double) Math.round(g);
        g /= 100; 
        Chunk tabela2dopis=new Chunk(String.format( "%.2f", e )+" "+Interfejs.waluta.getSelectedItem()+"\n"+String.format( "%.2f", f )+" "+Interfejs.waluta.getSelectedItem()+"\n"+String.format( "%.2f", g )+" "+Interfejs.waluta.getSelectedItem()+"\n",chude);

        tabela2dane2 = new PdfPCell(new Phrase(tabela2dopis));
        tabela2dane1.setBorder(Rectangle.NO_BORDER);
        tabela2dane2.setBorder(Rectangle.NO_BORDER);
        tabela2dane2.setHorizontalAlignment(Element.ALIGN_RIGHT);
        
        tabela2.addCell(tabela2dane1);
        tabela2.addCell(tabela2dane2);
        tabela2.setWidthPercentage(288 / 5.23f);
        tabela2.setWidths(new int[]{8,4});
        
        PdfPTable tabela3=new PdfPTable(2);
        tabela3.setHorizontalAlignment(Element.ALIGN_LEFT);
        PdfPCell tabela3dane1 = new PdfPCell();
        PdfPCell tabela3dane2 = new PdfPCell();
        Chunk tabela3dopis1=new Chunk(e.toString()+"\n"+f.toString()+"\n"+g.toString()+"\n",chude);

        double doZaplaty = g - (Double) Interfejs.zali.getValue();
        double zaliczka = (Double) Interfejs.zali.getValue();
        String liczba1 [] = String.format( "%.2f", doZaplaty ).split(",");
        String liczba2 [] = String.format( "%.2f", zaliczka ).split(",");

        String li = Slowo.translacja(Long.parseLong(liczba1[0]));
        String li2 = Slowo.translacja(Long.parseLong(liczba1[1]));
        
        String li3 = Slowo.translacja(Long.parseLong(liczba2[0]));
        String li4 = Slowo.translacja(Long.parseLong(liczba2[1])*1);
        
        PdfPTable tabela4=new PdfPTable(2);
        tabela4.setHorizontalAlignment(Element.ALIGN_LEFT);
        PdfPCell tabela4dane1 = new PdfPCell();
        PdfPCell tabela4dane2 = new PdfPCell();
        PdfPCell tabela4dane3 = new PdfPCell();
        PdfPCell tabela4dane4 = new PdfPCell();

        String grosze="";
        if (Interfejs.waluta.getSelectedItem()=="PLN") {
        	grosze = "gr";
        }
        else if (Interfejs.waluta.getSelectedItem()=="EUR"){
        	grosze="cent.";
        }


        Chunk dopis26=new Chunk("\n\n"+String.format( "%.2f", doZaplaty )+"\nS�ownie: "+li+" "+Interfejs.waluta.getSelectedItem()+" i "+li2+" "+grosze,chude);

        tabela4dane3 = new PdfPCell(new Phrase(dopis24));
        tabela4dane4 = new PdfPCell(new Phrase(dopis26));

        tabela4dane1.setBorder(Rectangle.NO_BORDER);
        tabela4dane2.setBorder(Rectangle.NO_BORDER);
        tabela4dane3.setBorder(Rectangle.NO_BORDER);
        tabela4dane4.setBorder(Rectangle.NO_BORDER);

        tabela4.addCell(tabela4dane1);
        tabela4.addCell(tabela4dane2);
        tabela4.addCell(tabela4dane3);
        tabela4.addCell(tabela4dane4);

        tabela4.setWidthPercentage(288 / 5f);
        tabela4.setWidths(new int[]{8,9});
        
        PdfPTable tabela5=new PdfPTable(2);
        PdfPCell tabela5dane1 = new PdfPCell();   
        tabela5dane1 = new PdfPCell(new Phrase(dopis27));
        PdfPCell tabela5dane2 = new PdfPCell();   
        tabela5dane2 = new PdfPCell(new Phrase(dopis28));
        tabela5.addCell(tabela5dane1);
        tabela5.addCell(tabela5dane2);
        tabela5.setTotalWidth(490);
        tabela5.setLockedWidth(true);
        
        tabela1.addCell(tabeladane);
        //tabela1.addCell(tabeladane333);
         tabela1.addCell(tabeladane2);
        

        //tabela1.addCell(dopis10+);
        tabela1.addCell(tabeladane3);
       // tabela1.addCell(tabeladane33);
       tabela1.addCell(tabeladane4);

        wpisywanie.add(dopis1);
        wpisywanie.add(dopis3);
        wpisywanie.add(dopis5);

        
        wpisywanie.add("______________________________________________________________________________\n\n");
        wpisywanie.add(tabela1);
        wpisywanie.add("\n");
        wpisywanie.add(table);
        wpisywanie.add("\n");
        wpisywanie.add(tabela2);
        wpisywanie.add(dopis7);
        wpisywanie.add(dopis8);
        wpisywanie.add("\n");
        wpisywanie.add(nowy);
        wpisywanie.add(nowy2);

        wpisywanie.add("\n\n");

        wpisywanie.add(dopis9);
        wpisywanie.add(dopis10);
        wpisywanie.add(dopis99);
        wpisywanie.add(dopis992);
        wpisywanie.add(tabela4);
        wpisywanie.add("\n");
        wpisywanie.add(tabela5);
        int k = 0;
        int check = 0;
        
        ResultSet result = null;
		try {
			result = EGGtura.stat.executeQuery("SELECT * FROM kontrahent where tytul='"+Interfejs.imie.getText()+"'");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        try {
			while (result.next()){
				k++;
			}
			if (k==0){
			       	 PreparedStatement prepStmt = null;
						try {
							prepStmt = EGGtura.conn.prepareStatement(
							        "insert into kontrahent values (NULL, ?, ?, ?);");
						} catch (SQLException e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}
			         try {
							prepStmt.setString(1, Interfejs.imie.getText());
							prepStmt.setString(2, Interfejs.adres.getText());
			         prepStmt.setString(3, Interfejs.nip.getText());
			     
			         	prepStmt.execute();
try {
			new PobierzKontrahent();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
			         
			        }
			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        dokument.add(new Paragraph(wpisywanie));

       // dokument.add(tabela1);

 
        Progres.frame.setVisible(false);
        dokument.close(); // no need to close PDFwriter?
        
    } catch (DocumentException e) {
        e.printStackTrace();
    } catch (FileNotFoundException e) {
        e.printStackTrace();
    }

}
}