package program;
 
public class Kontrahent {
    private int id;
    private String tytul;
    private String adres;
    private String nip;

 
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getTytul() {
        return tytul;
    }
    public void setTytul(String tytul) {
        this.tytul = tytul;
    }
    public String getAdres() {
        return adres;
    }
    public void setAdres(String adres) {
        this.adres = adres;
    }
    public String getNip() {
        return nip;
    }
    public void setNip(String nip) {
        this.nip = nip;
    }
    public Kontrahent() {}
    public Kontrahent(int id, String tytul, String adres, String nip) {
        this.id = id;
        this.tytul = tytul;
        this.adres = adres;
        this.nip = nip;

    }
 
    @Override
    public String toString() {
        return "["+id+"] - "+tytul+" - "+adres+"-"+nip;
    }
}