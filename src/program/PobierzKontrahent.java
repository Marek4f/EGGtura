package program;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import eggtura.EGGtura;
import program.Kontrahent;
public class PobierzKontrahent {
	public static ArrayList<String> tytul = new ArrayList<String>();
	public static ArrayList<String> adres = new ArrayList<String>();
	public static ArrayList<String> nip = new ArrayList<String>();
	public static ArrayList<Integer> id = new ArrayList<Integer>();
	public static List<Kontrahent> kontrahent = new LinkedList<Kontrahent>();
	public PobierzKontrahent() throws IOException{
			ResultSet result = null;
			try {
				result = EGGtura.stat.executeQuery("SELECT * FROM kontrahent");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			int l=0;
		    tytul.removeAll(tytul);
	         adres.removeAll(adres);
	         nip.removeAll(nip);
	         kontrahent.removeAll(kontrahent);
	            try {
					while(result.next()) {
					    id.add(result.getInt("id_kontrahent"));
					    tytul.add(result.getString("tytul"));
					    adres.add(result.getString("adres"));
					    nip.add(result.getString("nip"));
					    kontrahent.add(new Kontrahent(id.get(l), tytul.get(l), adres.get(l), nip.get(l)));
					    l++;
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            
	}
	
}
