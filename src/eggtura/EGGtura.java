
package eggtura;
import java.awt.*;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import javax.swing.*;

import com.sun.corba.se.pept.transport.Connection;

import program.Kontrahent;
import program.PlikOpcje;
import program.PobierzFirmy;
import program.PobierzKontrahent;
import interfejs.Interfejs;

import java.awt.event.*;
import java.beans.Statement;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
public class EGGtura {
	public static int n;
	public static ArrayList<String> listaproduktow = new ArrayList<String>();
	public static ArrayList<Double> listacen = new ArrayList<Double>();
	public static ArrayList<Double> listasztuk = new ArrayList<Double>();
	public static ArrayList<Integer> listavat = new ArrayList<Integer>();
	public static ArrayList<String> listajm = new ArrayList<String>();
	public static int numer=0;
    public static final String DRIVER = "org.sqlite.JDBC";
    public static final String DB_URL = "jdbc:sqlite:eggtura.db";

	public static java.sql.Connection conn;
    public static java.sql.Statement stat;
	public static void main(String[] args) throws IOException {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				 
				   try {
			            Class.forName(EGGtura.DRIVER);
			        } catch (ClassNotFoundException e) {
			            System.err.println("Brak sterownika JDBC");
			            e.printStackTrace();
			        }
			 
			        try {
			        	conn = DriverManager.getConnection(DB_URL);
			            stat = conn.createStatement();
			        } catch (SQLException e) {
			            System.err.println("Problem z otwarciem polaczenia");
			            e.printStackTrace();
			        }
			 
			        String createKsiazki = "CREATE TABLE IF NOT EXISTS kontrahent (id_kontrahent INTEGER PRIMARY KEY AUTOINCREMENT, tytul varchar(255), adres varchar(255), nip varchar(255))";
			        String createFirma = "CREATE TABLE IF NOT EXISTS firma (id_firma INTEGER PRIMARY KEY AUTOINCREMENT, nazwa varchar(255), adres varchar(255), nip varchar(255), telefon varchar(255), mail varchar(255), bank varchar(255), bank_nr varchar(255))";

			        try {
			        	stat.execute(createKsiazki);
			        	stat.execute(createFirma);

			        	} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	 
			        

		         	try {
						new PobierzFirmy();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
		         	try {
						new PobierzKontrahent();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				new Interfejs();

				try {
					new PlikOpcje();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			}
		});
	}

	}


